var app = angular.module("jdeveloper", ['ngRoute']);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    $routeProvider
    .when('/', {
        templateUrl: 'main.html',
        controller: 'MainCtrl'
    })
    .when('/install', {
        templateUrl: 'install.html',
        controller: 'MainCtrl'
    })
    .when('/tune', {
        templateUrl: 'tune.html',
        controller: 'MainCtrl'
    })
    .when('/study', {
        templateUrl: 'study.html',
        controller: 'MainCtrl'
    })
    .when('/weblogic', {
        templateUrl: 'weblogic.html',
        controller: 'MainCtrl'
    })
    .when('/uninstall', {
        templateUrl: 'uninstall.html',
        controller: 'MainCtrl'
    })
    .when('/errors', {
        templateUrl: 'errors.html',
        controller: 'MainCtrl'
    })
    .when('/contacts', {
        templateUrl: 'contacts.html',
        controller: 'MainCtrl'
    })
    .otherwise({redirectTo:'/main'});
}]);


app.controller('MainCtrl', ['$scope', '$http', function($scope, $http){

}]);
